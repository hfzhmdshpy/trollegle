package anon.trollegle;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.function.Predicate;

import static anon.trollegle.Util.t;

public class Commands {

    public final Multi m;
    
    public Commands(Multi m) {
        this.m = m;
        addAll();
    }
    
    public interface Body {
        public void process(String[] args, MultiUser target);
    }

    public static abstract class Command implements Body {

        public String usage;
        public final String helpstring;

        public Command(String name, String helpstring) {
            this.usage = name;
            this.helpstring = helpstring;
        }

        public String getHelpString(MultiUser target) {
            return this.helpstring;
        }

        public abstract void process(String[] args, MultiUser target);
    }
    
    public static class BodyCommand extends Command {
        public final Body body;
        public final int arglen;
        public BodyCommand(String name, String helpstring, int arglen, Body body) {
            super(name, helpstring);
            this.body = body;
            this.arglen = arglen;
        }
        public void process(String[] args, MultiUser target) {
            if (args.length < arglen)
                if (usage != null && usage.contains(" "))
                    target.schedTell(t("usage", usage));
                else
                    target.schedTell("Not enough arguments (need " + arglen + ")");
            else
                body.process(args, target);
        }
    }
    
    public static class BodySetting extends BodyCommand {
        public final Function<MultiUser, String> help;
        public BodySetting(String name, Function<MultiUser, String> help, int arglen, Body body) {
            super(name, null, arglen, body);
            this.help = help;
        }
        public String getHelpString(MultiUser target) {
            return help.apply(target);
        }
    }

    protected final LinkedHashMap<String, Command> commands = new LinkedHashMap<>();
    protected final LinkedHashMap<String, Command> aliases = new LinkedHashMap<>();
    
    protected Body forEachCommand(Predicate<String> action) {
        return (args, target) -> {
            String[] result = Util.filter(args, action);
            if (result.length < args.length)
                target.schedTell(t(args.length == 1 ? "nousershort" : "nousers"));
        };
    }

    public static String argsToString(int offset, String[] args) {
        StringBuilder sb = new StringBuilder();

        for (int i = offset; i < args.length; i++) {
            sb.append(args[i]).append(' ');
        }

        sb.setLength(Math.max(sb.length() - 1, 0)); //remove last space

        return sb.toString();
    }
    
    protected void addAll() { }

    protected void sanityCheck() throws RuntimeException {
        for (Entry<String, Command> entry : commands.entrySet()) {
            if (entry.getValue() == null) {
                throw new RuntimeException("[sc] Found a command with no body in commands! " + entry.getKey());
            }
            if (entry.getKey().contains(" ")) {
                throw new RuntimeException("[sc] Found a command with a space in it! " + entry.getKey());
            }
        }
    }
    
    public Command addCommand(String name, String usage, String help, int arglen, final Body body, String... aliases) {
        return addCommand(name, new BodyCommand(usage, help, arglen, body), aliases);
    }
    public Command addSetting(String name, String usage, Function<MultiUser, String> help, int arglen, final Body body, String... aliases) {
        return addCommand(name, new BodySetting(usage, help, arglen, body), aliases);
    }

    public Command addCommand(String name, Command c, String... aliases) {
        if (c.usage == null) {
            c.usage = name;
        }
        commands.put(name, c);
        if (aliases != null)
            for (String alias : aliases)
                this.aliases.put(alias, c);
        return c;
    }
    
    public Command getCommand(String cmd) {
        if (commands.containsKey(cmd))
            return commands.get(cmd);
        return aliases.get(cmd);
    }
    
}
