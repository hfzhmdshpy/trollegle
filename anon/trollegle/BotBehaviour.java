package anon.trollegle;

import java.io.*;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

import static anon.trollegle.MultiUser.LurkState;
import static anon.trollegle.Util.randomize;
import static anon.trollegle.Util.t;
import static anon.trollegle.Util.tn;

public class BotBehaviour extends UserBehaviour {

    public BotBehaviour(Multi m) {
        super(m);
    }

    protected void addAll() {
        super.addAll();

        addCommand("list", 0, (args, target) -> {
            String list;
            synchronized (m.users) {
                list = cookBotMessage(target, Util.mapHack(
                        "action", "user_list",
                        "users", m.allUsers.stream()
                            .filter(UserConnection::isReady)
                            .map(u -> Util.mapHack(
                                "id", u.getNumber(),
                                "name", u.getNick(),
                                "age", u.age() / 1000,
                                "you", u == target ? true : null,
                                "flags", m.appendFlags(target, u, null)))
                            .toArray()
                        ));
            }
            target.schedSend(list);
        });
        
        sanityCheck();
    }

    public void tellNickChanged(MultiUser target, MultiUser topic, String oldNick, boolean silent) {
        if (silent && (target != topic || !isPrivileged(target)))
            return;
        target.schedSend(cookBotMessage(target, Util.mapHack(
                "action", "update_list",
                "user", topic.getNumber(),
                "name", topic.getNick()
                )));
    }
    
    public void tellJoined(MultiUser target, MultiUser topic) {
        Map<String, Object> map = Util.mapHack(
                "action", "update_list",
                "user", topic.getNumber(),
                "connect", topic.isQuestionMode() ? "question" : 
                        topic.isPulseEver() ? "pulse" : "normal",
                "name", topic.getNick());
        if (topic.getQuestion() != null)
            map.put("question", topic.getQuestion());
        target.schedSend(cookBotMessage(target, map));
    }
    
    public void tellLeft(MultiUser target, MultiUser topic) {
        target.schedSend(cookBotMessage(target, Util.mapHack(
                "action", "update_list",
                "user", topic.getNumber(),
                "disconnect", Objects.toString(topic.getKickReason(), "normal")
                )));
    }
    
    public void updateLurk(MultiUser target, MultiUser topic) {
        LurkState lurkState = topic.lurkState();
        if (target == topic) {
            target.schedTell(t("lurk_" + lurkState));
            if (topic.shouldFamiliarizeLurker())
                target.schedTell(t("lurkhelp"));
        }
        if (lurkState != LurkState.PRELURKER)
            updateFlags(target, topic, null);
    }
    
    public void updateFlagsPrivileged(MultiUser target, MultiUser topic, String message) {
        if (isPrivileged(target))
            updateFlags(target, topic, message);
    }
    
    public void updateFlags(MultiUser target, MultiUser topic, String message) {
        target.schedSend(cookBotMessage(target, Util.mapHack(
                "action", "update_list",
                "user", topic.getNumber(),
                "flags", m.appendFlags(target, topic, null)
                )));
    }
    
    public String cookBotMessage(MultiUser target, Map<String, Object> message) {
        return "#A " + JsonValue.wrap(m.fillBotMessage(target, message));
    }
    
}
